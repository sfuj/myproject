# Calculate the sum, average, and standard deviation
# of the values in a text file, formatted as one value per line.
# written by M R on 2013 FEb 15


import sys
import math

values = []
for line in open(sys.argv[1]):
    value = float(line)
    values.append(value)

total = sum(values)
average = total / len(values)

diffSquared = []
for value in values:
    diff = value - average
    diffSquared.append(diff**2)

stdDev = math.sqrt(sum(diffSquared) / (len(values) - 1))

print len(values), 'Values were read in'
print 'The total of the input values is:', total
print 'The average of the input values is:', average
print 'The standard deviation of the input values is:', stdDev